console.log("Entered Content Script Analytics");
const myButton = document.querySelector("#CHECKOUT");
let basePath = 'http://localhost:9430/';//https://collect.intentx.com http://localhost:9420

let apiEndPoint = {
  pageView: basePath + 'cx/cashx/v1/visit-pv',
  linkClick: basePath + 'cx/cashx/v1/click',
};

let clickObj = {
    position: 0,
    affiliate: null,
    affiliateUrl: null,
    product: {
       visitInfo: {},
       pageViewInfo: {}
    }
};

let config = {
        client: document.location.hostname,
        site: document.location.hostname
    };

let visitObj = {
    visitId: getCookie('visitId'),
    visitorId: getCookie('visitorId'),
    source: {
        channel: null,
        referrer: document.referrer,
        mktEngine: null,
        mktCampaign: null,
        mktSubCampaign: null
    },
    device: {
        userAgent: navigator.userAgent,
        cookieEnabled: navigator.cookieEnabled,
        jsEnabled: true
    }
};

let pageViewObj = {
    pageType: "RUP",
    url: document.location.href,
    referringUrl: document.referrer,
    products: []
};

function setCookie(name, value, expiryDays) {  // Cookie helper
    let date = new Date();
    date.setTime(date.getTime() + (expiryDays*24*60*60*1000));
    let expires = "expires="+ date.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

function getCookie(name) {  // Cookie helper
    let result = document.cookie.match(new RegExp(name + '=([^;]+)'));
    return !!result ? result[1] : null;
}

function makeAPIcall (url, params, successCallback, errorCallback) { // API helper
    let xhr = null;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhr.open('POST', url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
        if (this.readyState === this.DONE) {
            if (this.status >= 200 && this.status < 300) {
                if (typeof successCallback === 'function') {
                    successCallback.call(null, JSON.parse(this.responseText));
                }
            } else if (typeof errorCallback === 'function') {
                errorCallback();
            }
        }
    };
    xhr.onerror = function () {
        if (typeof errorCallback === 'function') {
            errorCallback();
        }
    }
    xhr.send(JSON.stringify(params || {}));
}

function makeAPIcall (url, params, successCallback, errorCallback) { // API helper
    let xhr = null;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhr.open('POST', url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
        if (this.readyState === this.DONE) {
            if (this.status >= 200 && this.status < 300) {
                if (typeof successCallback === 'function') {
                    successCallback.call(null, JSON.parse(this.responseText));
                }
            } else if (typeof errorCallback === 'function') {
                errorCallback();
            }
        }
    };
    xhr.onerror = function () {
        if (typeof errorCallback === 'function') {
            errorCallback();
        }
    }
    xhr.send(params || {});
}


function getUrl(option){
    'use strict';
    switch(option){
        case "visit-pv":{
            return apiEndPoint.pageView;
        } break;
        default: {
            return apiEndPoint.pageView;
        }
    }
}



async function pageView() {
    'use strict';
    console.log("I am being loaded");
    
    visitObj.visitId = getCookie('visitId');
    visitObj.visitorId = getCookie('visitorId');
    visitObj.client = config.client.slice(4,config.client.length-4);
    visitObj.site = config.site;

    let body_temp = JSON.stringify({visit: { visitInfo: visitObj }, pageView: { pageViewInfo: pageViewObj } });
    await makeAPIcall(getUrl("visit-pv"), body_temp, 
        function(response){
        if (response && response.visit && response.visit.visitInfo) {
                setCookie('visitorId', response.visit.visitInfo.visitorId, 365);
                setCookie('visitId', response.visit.visitInfo.visitId, 30);
                setCookie('cxVisitorId', response.visit.visitInfo.ixVisitorId || null, 365);
            }
            if (response && response.pageView && response.pageView.pageViewInfo) {
                setCookie('pageViewId', response.pageView.pageViewInfo.pageViewId, 30);
            }
        }, 
        function(error){
            console.log(error);
        }
    );
}

document.addEventListener("onload", pageView(), true);


function getTrackingDimension() {
    'use strict';
    return "1px";
}

function getIdOfTracker() {
    'use strict';
    return "iTrack";
}

function createTrackingImg(url) {
    'use strict';
    let trackingImg = document.createElement("IMG");
    trackingImg.setAttribute("id", getIdOfTracker());
    trackingImg.setAttribute("src", url);
    trackingImg.setAttribute("width", getTrackingDimension());
    trackingImg.setAttribute("height", getTrackingDimension());
    return trackingImg;
}

function createTrackingPixelInDOM(url) {
    'use strict';
    document.body.appendChild(createTrackingImg(url));
}

function addEvent(elem, eventType, listener){
    if (window.addEventListener) {
        elem.addEventListener(eventType, listener, false);
    } else if (window.attachEvent) {
        elem.attachEvent('on' + eventType, listener);
    } else {
        eventType = 'on' + eventType;
        elem[eventType] = listener;
    }
}

function clickAnalytics(){
    clickObj.pageViewInfo = {pageViewId: getCookie('pageViewId')};
    clickObj.visitInfo = {visitId: getCookie('visitId'), visitorId: getCookie("visitorId"), ixVisitorId: getCookie("cxVisitorId"), client: config.client};
    addEvent(myButton, "click", function(event){
        let link = event.currentTarget.getAttribute('href');
        clickObj.affiliateUrl = link;
        clickObj.position = myButton.getAttribute('data-position') || "";
        if(link && clickObj.visitInfo && clickObj.pageViewInfo){
            makeAPIcall("http://localhost:9430/cx/cashx/v1/click", clickObj, 
                function(response){
                    var clicksCookie = getCookie('clicks');
                    if (clicksCookie) {
                      clicksCookie += ','+ response.clickId;
                    } else {
                      clicksCookie = response.clickId;
                    }
                    setCookie('clicks', clicksCookie, 30);
                    document.location.href = link;
            }, function (){
                    document.location.href = link;
                }
            );
            event.preventDefault();
        }
    });
}

chrome.runtime.onConnect.addListener(function (port) {
    'use strict';
    console.log('object :');
    port.onMessage.addListener(function (msg) {
        switch (msg.action) {
        case "click":
            console.log("clicked");
            createTrackingPixelInDOM(msg.affiliateUrl);
            //clickAnalytics();
            break;
        }

    });
});

