chrome.runtime.onInstalled.addListener(function () {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
    chrome.declarativeContent.onPageChanged.addRules([{
      conditions: [
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.macys.' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.nordstrom.' }
        }),
        new chrome.declarativeContent.PageStateMatcher({

         pageUrl: { hostContains: '.walmart.'}
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.wayfair.'}
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.hayneedle.' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
         pageUrl: { hostContains: '.bloomingdales.'}
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.houzz.'}
        }),
        new chrome.declarativeContent.PageStateMatcher({
         pageUrl: { hostContains: '.crateandbarrel.'}
        }),
        new chrome.declarativeContent.PageStateMatcher({
         pageUrl: { hostContains: '.qvc.', pathContains: 'checkout' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.williams-sonoma.', pathContains: 'payment' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.hsn.', pathContains: 'order-review' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.jcpenny.', pathContains: 'cart' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.blair.', pathContains: 'checkout' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.fanatics.', pathContains: 'cart' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.ballarddesigns.', pathContains: 'ShoppingCartView' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.webstaurantstore.', pathContains: 'viewcart' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.boscovs.', pathContains: 'shop' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.nflshop.', pathContains: 'cart' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.campsaver.', pathContains: 'cart' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostContains: '.bedbathandbeyond.' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
         pageUrl: { hostContains: '.homedepot.' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
         pageUrl: { hostContains: '.surlatable.', pathContains: 'art' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
         pageUrl: { hostContains: '.neimanmarcus.', pathContains: 'art' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
         pageUrl: { hostContains: '.farfetch.', pathContains: 'Payment' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
         pageUrl: { hostContains: '.overstock.', pathContains: 'checkout' }
        }),
        new chrome.declarativeContent.PageStateMatcher({
         pageUrl: { hostContains: '.wolfandbadger.', pathContains: 'shopping-bag' }
        })

      ],
      // ... show the page action.
      actions: [new chrome.declarativeContent.ShowPageAction()]
    }]);
  })

});

