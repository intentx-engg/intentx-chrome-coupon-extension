console.log("Entered Content script functionality");

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}



chrome.runtime.onConnect.addListener(function (port) {
    'use strict';
    console.log('object :');
    port.onMessage.addListener(function (msg) {
        switch (msg.action) {
        case "apply_coupon":
            console.log(msg)
            let applyCoupon = new Function("return " + msg.applyCoupon)();
            if (msg.couponCode.length > 0){
                applyCoupon(msg.couponCode);
            }
            port.postMessage(msg);
            break;
        case "check_status":
            console.log(msg)
            let checkStatus = new Function("return " + msg.checkStatus)();
            checkStatus(msg);
            port.postMessage(msg);
            break;
        case "trackingPixel":
            createTrackingPixelInDOM(msg.url);
            break;
        case "all_coupons_applied":
            port.postMessage(msg);
            break;
        }

    });
});
