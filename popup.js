let myButton = document.getElementById("applyCoupons");
let myclose = document.getElementById("close");
let couponObject = {};
let couponStatuses = [];
let currentHost;
let currentUrl;
// var clickObj = {
//         position: 0,
//         affiliate: null,
//         affiliateUrl: null,
//         product: {
//            visitInfo: {},
//            pageViewInfo: {}
//         }
//     };

async function getIntialRes() {
  const response = await fetch(getCouponURL());
  const couponFetchResponse = await response.json();
  setBadgeText(couponFetchResponse.coupons.length);
  await createIntialCouponStatus(couponFetchResponse, currentUrl);
}

function getCouponURL(){
  let hostarray = currentHost.split(".")
  let store = ""
  if (hostarray[0] == "www"){
    store = hostarray[1]
  }else{
    store = hostarray[0]
  }
  return `http://localhost:9422/ix/analysis/v1/coupon/coupons?store=${store}`;//`https://service.intentx.com/ix/analysis/v1/coupon/coupons?store=${host}`;
}

function setBadgeText(coupons){
  if (coupons === 0){
    chrome.browserAction.setBadgeText({ text: "*" });
  }else{
    chrome.browserAction.setBadgeText({ text: `${coupons}` });
  }
}

async function createIntialCouponStatus(couponsRes, url) {
  hideElement("applyCoupons");
  if (await couponsRes.coupons.length > 0) {
    if ( url.pathname.match(/checkout/) || 
          url.pathname.match(/my-bag/) || 
          url.pathname.match(/shopping-bag/) )  {

      showElement("applyCoupons");
    } else {
      setInitialTextInPopUp(couponsRes.coupons.length);
    }
  } else {
    setInitialTextInPopUp();
  }
}

function hideElement(elementId){
  document.getElementById(elementId).style.display = 'none';
}

function showElement(elementId){
  document.getElementById(elementId).style.display = "inline-block";
}

function setInitialTextInPopUp(lenOfCoupons=0){
  let status = document.createElement("P");
  status.innerHTML = (lenOfCoupons !== 0) ? `Yaay! We Found ${lenOfCoupons} coupons for ${currentHost}`
                                            : `Sorry, we could not find any coupons for ${currentHost}`;
  appendToCouponStatuses(status);
}

function appendToCouponStatuses(element){
  document.getElementById("couponStatuses").appendChild(element);
}

// Controller to decide on the actions based on message to and from content script.
async function controller(aMessage) {
  switch (aMessage.action) {
    case "apply_coupon":
      messageSender({ action: "check_status", couponCode: aMessage.couponCode, attempt: 1, checkStatus: aMessage.checkStatus });
      break;
    case "check_status":
      if (aMessage.attempt < 3 && aMessage.success != true) {
        sleep(7000);
        aMessage.attempt += 1;
        messageSender(aMessage);
      } else {
        console.log("Coupon statuses: ", couponStatuses);
        delete aMessage.attempt;
        delete aMessage.checkStatus;
        delete aMessage.action;
        delete aMessage.success;
        delete aMessage.statusMessage;
        couponStatuses.push(aMessage);
        sendNextCoupon();
      }
     break;
    case "all_coupons_applied":
      console.log("Coupon statuses: ", couponStatuses);
      createStatusList();
      break;
    case "visiting":
      console.log("Visit Information: ", aMessage.visitInfo);
      break;
  }
}

function createStatusList() {
  let statusList = document.createElement("UL");
  couponStatuses.forEach(function (status) {
    let listItem = document.createElement("LI");
    let innerList = document.createElement("UL");
    for (let key of Object.keys(status)) {
      let innerListItem = document.createElement("LI");
      innerListItem.innerHTML = `${key}: ${status[key]}`;
      innerList.appendChild(innerListItem);
    }
    listItem.appendChild(innerList);
    statusList.appendChild(listItem);
  });
  appendToCouponStatuses(statusList);
}

function disableButton(btnId){
  document.getElementById(btnId).disabled = true;
}

function messageListener(receivedMessage) {
  controller(receivedMessage);
}

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}

function messageSender(sendingMessage) {

  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    const port = chrome.tabs.connect(tabs[0].id);
    port.postMessage(sendingMessage);
    port.onMessage.addListener((receivedMessage) => { messageListener(receivedMessage) });
  });
}

//https://service.intentx.com/
async function fetchCoupons() {
  const host = currentHost;
  console.log("CurrentHost: ", host);
  const response = await fetch(getCouponURL());
  const couponsRes = await response.json();
  let coupons = [];
  console.log(couponsRes);
  couponObject.checkStatus = couponsRes.checkStatus;
  couponObject.applyCoupon = couponsRes.applyCoupons;
  couponsRes.coupons.forEach(function (coupon) {
    coupons.push(coupon);
  });
  couponObject.coupons = coupons;
}

async function applyCookie() {
  let sendingMessage = {
    "action": "trackingPixel",
    "url": "https%3A%2F%2Fclick.linksynergy.com%2Ffs-bin%2Fclick%3Fid%3D00003267884%26offerid%3D764406.100452572%26type%3D3"
  };
  messageSender(sendingMessage);
}

function setCookie(name, value, expiryDays) {  // Cookie helper
    let date = new Date();
    date.setTime(date.getTime() + (expiryDays*24*60*60*1000));
    let expires = "expires="+ date.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

function getCookie(name) {  // Cookie helper
    let result = document.cookie.match(new RegExp(name + '=([^;]+)'));
    return !!result ? result[1] : null;
}

function addEvent(elem, eventType, listener) {
    if (window.addEventListener) {
        elem.addEventListener(eventType, listener, false);
    } else if (window.attachEvent) {
        elem.attachEvent('on' + eventType, listener);
    } else {
        eventType = 'on' + eventType;
        elem[eventType] = listener;
    }
}

function makeAPIcall (url, params, successCallback, errorCallback) { // API helper
    let xhr = null;
    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    } else {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhr.open('POST', url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
        if (this.readyState === this.DONE) {
            if (this.status >= 200 && this.status < 300) {
                if (typeof successCallback === 'function') {
                    successCallback.call(null, JSON.parse(this.responseText));
                }
            } else if (typeof errorCallback === 'function') {
                errorCallback();
            }
        }
    };
    xhr.onerror = function () {
        if (typeof errorCallback === 'function') {
            errorCallback();
        }
    }
    xhr.send(params || {});
}

async function sendClick(url) { 
  let msg = {};
  msg.action = "click";
  msg.affiliateUrl = url;

  messageSender(msg);
  // clickObj.pageViewInfo = {pageViewId: getCookie('pageView')};
  // clickObj.visitInfo = {visitId: getCookie('visit'), visitorId: utils.getCookie("visitorId"), ixVisitorId: utils.getCookie("cxVisitor"), client: currentHost};
  // addEvent(myButton, "click", function(event){
  //   let link = event.currentTarget.getAttribute('href');
  //   clickObj.affiliateUrl = link;
  //   clickObj.position = myButton.getAttribute('data-position') || "";
  //   if(link && clickObj.visitInfo && clickObj.pageViewInfo){
  //     makeAPIcall("http://localhost:9430/cx/cashx/v1/click", clickObj, function(response){
  //       var clicksCookie = utils.getCookie('clicks');
  //       if (clicksCookie) {
  //         clicksCookie += ','+ response.clickId;
  //       } else {
  //         clicksCookie = response.clickId;
  //       }
  //       setCookie('clicks', clicksCookie, 30);
  //       document.location.href = link;
  //     }, function (){
  //        document.location.href = link;
  //     });
  //     event.preventDefault();
  //   }
  // });
}


myButton.onclick = async function () {
  //await sendClick();
  document.getElementById("couponStatuses").innerHTML = "";
  couponObject = {};
  couponObject.coupons = [];
  couponStatuses = [];
  await fetchCoupons();
  console.log("Coupons and functions: ")
  console.log(couponObject);
  await sendNextCoupon();
};

async function sendNextCoupon() {
  let coupons = couponObject.coupons;
  let myCoup = coupons && coupons.pop();
  let message = {};

  if (myCoup) {
    // message.couponCode = myCoup.sourceCoupon.couponCode;
    // message.applyCoupon = couponObject.applyCoupon;
    // message.checkStatus = couponObject.checkStatus;
    // message.affiliateUrl = myCoup.sourceCoupon.affiliateUrl;
    message.couponCode = myCoup.couponCode;
    message.affiliateUrl = myCoup.affiliateUrl;
    message.trackingPixel = myCoup.trackingPixel;
    message.applyCoupon = couponObject.applyCoupon;
    message.checkStatus = couponObject.checkStatus;
    console.log(message)
    //await sendClick(message.affiliateUrl);
    applyCookie();
    message.action = "apply_coupon";
  } else {
    message = { action: "all_coupons_applied" }
  }
  console.log(message);
  messageSender(message);
}

myclose.onclick = function () {
  window.close();
}




// Behaviour

chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) { //'windowId': chrome.windows.WINDOW_ID_CURRENT},
    currentUrl = new URL(tabs[0].url);
    currentHost = currentUrl.hostname;
    getIntialRes();
});


